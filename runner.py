import argparse
import json
import shutil
import subprocess
from datetime import datetime
from os import mkdir, path, rmdir

import numpy
import torch

from mysac.batch.numpy_batch import (NumpySampledBuffer,
                                     NumpySampledBufferForRNN)
from NAO_SAC import NAOBallEnv
from mysac.evaluators.sac_evaluator import SACEvaluator
from mysac.sac.sac import SACAgent
from mysac.trainers.generic_train import generic_train
from mysac.models.mlp import PolicyModel, QModel

def create_folders(experiment_folder: str):
    """
    Create the folder structure for the experiment

    Args:
        experiment_folder: the path where the folder structure will be created

    Raises:
        FileExistsError: if the structure already exists
    """
    if path.isdir(experiment_folder + '/models'):
        confirmation = input('Experiment already exists! Override? [y/N]: ')

        if confirmation == 'n':
            raise ValueError('Pick a different folder for the experiment '
                             'artifacts')

        else:
            shutil.rmtree(experiment_folder + '/models/', ignore_errors=True)
            shutil.rmtree(experiment_folder + '/stats/', ignore_errors=True)

    mkdir(experiment_folder + '/models/')
    mkdir(experiment_folder + '/stats/')


def run_experiment(experiment_folder: str):
    """ Run an experiment from a dictionaty of specifications

    Args:
        experiment_folder: the path to the experiment folder. If does not
            exists, it is created. If it already exists, we try to train
            with specs as follows it. """
    create_folders(experiment_folder)

    meta = {
        'branch': subprocess.check_output(
            ["git", "branch"]).decode(),
        'commit': subprocess.check_output(
            ["git", "rev-parse", "--short", "HEAD"]).decode(),
        'date': datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
    }

    # Fixes the random seeds
    torch.manual_seed(0)
    numpy.random.seed(0)

    env = NAOBallEnv(experiment_folder)

    num_obs = env.observation_space.shape[0]
    num_actions = env.action_space.shape[0]

    buffer_params = dict(
        size=1000000,
        observation_size=num_obs,
        action_size=num_actions
    )

    # Select the model
    buffer = NumpySampledBuffer(**buffer_params)

    model_params = dict(
        num_inputs=num_obs,
        num_actions=num_actions,
        hidden_sizes=256
    )

    policy = PolicyModel(**model_params)
    q1_model = QModel(**model_params)
    q1_target = QModel(**model_params)
    q2_model = QModel(**model_params)
    q2_target = QModel(**model_params)

    print('Policy:', policy)

    agent = SACAgent(
        # Env
        env=env,

        # Models
        policy_model=policy,
        q1_model=q1_model,
        q2_model=q2_model,
        q1_target=q1_target,
        q2_target=q2_target,

        # Hyperparams
        gamma=0.99,
        policy_lr=3e-4,
        q_lr=3e-4,
        alpha_lr=3e-4,
        tau=5e-3
    )

    try:
        generic_train(
            env=env,
            agent=agent,
            buffer=buffer,
            experiment_folder=experiment_folder,
            evaluator=SACEvaluator(experiment_folder),
            num_epochs=4000,
            batch_size=512,
            max_steps_per_episode=250,
            sampled_steps_per_epoch=500,
            train_steps_per_epoch=125
        )

    except KeyboardInterrupt:
        meta['end_date'] = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")

        with open(experiment_folder + '/meta.json', 'w') as meta_file:
            json.dump(meta, meta_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run SAC from specifications')

    parser.add_argument('--exp_path', type=str,
                        help='Output path for model binaries and stats')
    parser.add_argument('--deterministic', action='store_true',
                        help='Run the deterministic algorithm')
    parser.add_argument('--viz', action='store_true')

    args = parser.parse_args()

    if not args.deterministic:
        run_experiment(args.exp_path)
